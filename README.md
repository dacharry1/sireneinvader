# sireneinvader

## Comment utiliser le projet

1. L'application démarre avec `main.js`. Il se connecte à **PM2** et lance le **bus PM2** pour écouter les messages des workers. Le nombre de workers est défini par le nombre de coeur du processeur de la machine utilisée pour exécuter le code et un nombre de **nodes** défini dans le main.js. 

2. Il vérifie la présence du fichier `StockEtablissement.csv` à la racine du projet. Si le fichier **CSV** spécifié existe, la division du fichier commence.

3. Chaque **worker** exécute `worker.js`. Il envoie un message au **process parent** pour indiquer qu'il est prêt.

4. Le **process parent** envoie un message à chaque **worker** avec le nom du **fichier** qu'il doit traiter.

5. Le **worker** lit le fichier, **traite** les données et les **insère** dans la base de données. Une fois terminé, il **envoie** un message au **processus parent** pour indiquer qu'il a terminé.

6. Le **processus parent** met à jour le statut du **worker** et lui assigne un nouveau **travail** si disponible.

7. L'application continue de tourner jusqu'à ce que tous les fichiers aient été insérés dans la base de données.
